import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult } from 'sigmasoft-ts/models';
import { Message }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class MessageService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getMessages(): Observable<OperationResult<Message[]>> {
    return this.http.get<OperationResult<Message[]>>(this.configService.configuration.api.message).map(x => { x.data = Convert.toObject(x.data, Message); return x; });
  }
  getMessageById(id: number): Observable<OperationResult<Message>> {
    return this.http.get<OperationResult<Message>>(this.configService.configuration.api.message + id).map(x => { x.data = Convert.toObject(x.data, Message); return x; });
  }
  insertMessage(obj: Message): Observable<OperationResult<Message>> {
    return this.http.post<OperationResult<Message>>(this.configService.configuration.api.message, obj);
  }
  updateMessage(obj: Message): Observable<OperationResult<Message>> {
    return this.http.patch<OperationResult<Message>>(this.configService.configuration.api.message, obj);
  }
  deleteMessageById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.message + id);
  }
}
