import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult } from 'sigmasoft-ts/models';
import { Category }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class CategoryService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getCategorys(): Observable<OperationResult<Category[]>> {
    return this.http.get<OperationResult<Category[]>>(this.configService.configuration.api.category).map(x => { x.data = Convert.toObject(x.data, Category); return x; });
  }
  getCategoryById(id: number): Observable<OperationResult<Category>> {
    return this.http.get<OperationResult<Category>>(this.configService.configuration.api.category + id).map(x => { x.data = Convert.toObject(x.data, Category); return x; });
  }
  insertCategory(obj: Category): Observable<OperationResult<Category>> {
    return this.http.post<OperationResult<Category>>(this.configService.configuration.api.category, obj);
  }
  updateCategory(obj: Category): Observable<OperationResult<Category>> {
    return this.http.patch<OperationResult<Category>>(this.configService.configuration.api.category, obj);
  }
  deleteCategoryById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.category + id);
  }
}
