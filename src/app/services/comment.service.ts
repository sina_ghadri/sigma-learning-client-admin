import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult } from 'sigmasoft-ts/models';
import { Comment }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class CommentService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getComments(): Observable<OperationResult<Comment[]>> {
    return this.http.get<OperationResult<Comment[]>>(this.configService.configuration.api.comment).map(x => { x.data = Convert.toObject(x.data, Comment); return x; });
  }
  getCommentById(id: number): Observable<OperationResult<Comment>> {
    return this.http.get<OperationResult<Comment>>(this.configService.configuration.api.comment + id).map(x => { x.data = Convert.toObject(x.data, Comment); return x; });
  }
  insertComment(obj: Comment): Observable<OperationResult<Comment>> {
    return this.http.post<OperationResult<Comment>>(this.configService.configuration.api.comment, obj);
  }
  updateComment(obj: Comment): Observable<OperationResult<Comment>> {
    return this.http.patch<OperationResult<Comment>>(this.configService.configuration.api.comment, obj);
  }

  approveComment(id: number): Observable<OperationResult<Comment>> {
    return this.http.put<OperationResult<Comment>>(this.configService.configuration.api.comment + `${id}/approve`, {});
  }
  rejectComment(id: number, reason: string): Observable<OperationResult<Comment>> {
    return this.http.put<OperationResult<Comment>>(this.configService.configuration.api.comment + `${id}/reject`, reason);
  }

  deleteCommentById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.comment + id);
  }
}
