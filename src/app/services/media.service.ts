import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult } from 'sigmasoft-ts/models';
import { Media }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class MediaService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getMedias(): Observable<OperationResult<Media[]>> {
    return this.http.get<OperationResult<Media[]>>(this.configService.configuration.api.media).map(x => { x.data = Convert.toObject(x.data, Media); return x; });
  }
  getMediaById(id: number): Observable<OperationResult<Media>> {
    return this.http.get<OperationResult<Media>>(this.configService.configuration.api.media + id).map(x => { x.data = Convert.toObject(x.data, Media); return x; });
  }
  insertMedia(obj: Media): Observable<OperationResult<Media>> {
    return this.http.post<OperationResult<Media>>(this.configService.configuration.api.media, obj);
  }
  updateMedia(obj: Media): Observable<OperationResult<Media>> {
    return this.http.patch<OperationResult<Media>>(this.configService.configuration.api.media, obj);
  }
  deleteMediaById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.media + id);
  }
}
