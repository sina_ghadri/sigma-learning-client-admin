import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult, KeyValue } from 'sigmasoft-ts/models';
import { Course, Post } from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class CourseService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  cast(obj: Course): Course {
    obj.post = Convert.toObject(obj.post, Post);
    return obj;
  }
  map(op: any): any {
    if (op.data) {
      op.data = Convert.toObject(op.data, Course);
      if (Array.isArray(op.data)) for (let i = 0; i < op.data.length; i++) op.data[i] = this.cast(op.data[i]);
      else op.data = this.cast(op.data);
    }
    return op;
  }

  getTypes(): KeyValue[] {
    return [
      { key: 0, value: 'رایگان' },
      { key: 1, value: 'اعضاء ویژه' },
      { key: 2, value: 'فروشی' },
    ];
  }
  getStatuses(): KeyValue[] {
    return [
      { key: 0, value: 'در صف تولید' },
      { key: 1, value: 'در حال بارگزاری' },
      { key: 2, value: 'تکمیل شده' },
      { key: 3, value: 'به تعویق افتاده' },
    ];
  }
  getCourses(): Observable<OperationResult<Course[]>> {
    return this.http.get<OperationResult<Course[]>>(this.configService.configuration.api.course).map(x => this.map(x));
  }
  getCourseById(id: number): Observable<OperationResult<Course>> {
    return this.http.get<OperationResult<Course>>(this.configService.configuration.api.course + id).map(x => this.map(x));
  }
  insertCourse(obj: Course): Observable<OperationResult<Course>> {
    return this.http.post<OperationResult<Course>>(this.configService.configuration.api.course, obj);
  }
  updateCourse(obj: Course): Observable<OperationResult<Course>> {
    return this.http.patch<OperationResult<Course>>(this.configService.configuration.api.course, obj);
  }
  deleteCourseById(id: number): Observable<OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.course + id);
  }
  //---------------------------
  getLessons(id: number): Observable<OperationResult<Course[]>> {
    return this.http.get<OperationResult<Course[]>>(this.configService.configuration.api.course + `${id}/lessons`).map(x => { x.data = Convert.toObject(x.data, Course); return x; });
  }
}
