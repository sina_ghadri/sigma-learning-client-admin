import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult } from 'sigmasoft-ts/models';
import { User }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getUsers(): Observable<OperationResult<User[]>> {
    return this.http.get<OperationResult<User[]>>(this.configService.configuration.api.user).map(x => { x.data = Convert.toObject(x.data, User); return x; });
  }
  getUserById(id: number): Observable<OperationResult<User>> {
    return this.http.get<OperationResult<User>>(this.configService.configuration.api.user + id).map(x => { x.data = Convert.toObject(x.data, User); return x; });
  }
  insertUser(obj: User): Observable<OperationResult<User>> {
    return this.http.post<OperationResult<User>>(this.configService.configuration.api.user, obj);
  }
  updateUser(obj: User): Observable<OperationResult<User>> {
    return this.http.patch<OperationResult<User>>(this.configService.configuration.api.user, obj);
  }
  deleteUserById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.user + id);
  }
}
