import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult } from 'sigmasoft-ts/models';
import { Lesson }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class LessonService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getLessons(): Observable<OperationResult<Lesson[]>> {
    return this.http.get<OperationResult<Lesson[]>>(this.configService.configuration.api.lesson).map(x => { x.data = Convert.toObject(x.data, Lesson); return x; });
  }
  getLessonById(id: number): Observable<OperationResult<Lesson>> {
    return this.http.get<OperationResult<Lesson>>(this.configService.configuration.api.lesson + id).map(x => { x.data = Convert.toObject(x.data, Lesson); return x; });
  }
  insertLesson(obj: Lesson): Observable<OperationResult<Lesson>> {
    return this.http.post<OperationResult<Lesson>>(this.configService.configuration.api.lesson, obj);
  }
  updateLesson(obj: Lesson): Observable<OperationResult<Lesson>> {
    return this.http.patch<OperationResult<Lesson>>(this.configService.configuration.api.lesson, obj);
  }
  deleteLessonById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.lesson + id);
  }
}
