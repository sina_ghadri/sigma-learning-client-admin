import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult } from 'sigmasoft-ts/models';
import { Discussion }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class DiscussionService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getDiscussions(): Observable<OperationResult<Discussion[]>> {
    return this.http.get<OperationResult<Discussion[]>>(this.configService.configuration.api.discussion).map(x => { x.data = Convert.toObject(x.data, Discussion); return x; });
  }
  getDiscussionById(id: number): Observable<OperationResult<Discussion>> {
    return this.http.get<OperationResult<Discussion>>(this.configService.configuration.api.discussion + id).map(x => { x.data = Convert.toObject(x.data, Discussion); return x; });
  }
  insertDiscussion(obj: Discussion): Observable<OperationResult<Discussion>> {
    return this.http.post<OperationResult<Discussion>>(this.configService.configuration.api.discussion, obj);
  }
  updateDiscussion(obj: Discussion): Observable<OperationResult<Discussion>> {
    return this.http.patch<OperationResult<Discussion>>(this.configService.configuration.api.discussion, obj);
  }
  deleteDiscussionById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.discussion + id);
  }
}
