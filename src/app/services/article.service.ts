import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult } from 'sigmasoft-ts/models';
import { Article }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class ArticleService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getArticles(): Observable<OperationResult<Article[]>> {
    return this.http.get<OperationResult<Article[]>>(this.configService.configuration.api.article).map(x => { x.data = Convert.toObject(x.data, Article); return x; });
  }
  getArticleById(id: number): Observable<OperationResult<Article>> {
    return this.http.get<OperationResult<Article>>(this.configService.configuration.api.article + id).map(x => { x.data = Convert.toObject(x.data, Article); return x; });
  }
  insertArticle(obj: Article): Observable<OperationResult<Article>> {
    return this.http.post<OperationResult<Article>>(this.configService.configuration.api.article, obj);
  }
  updateArticle(obj: Article): Observable<OperationResult<Article>> {
    return this.http.patch<OperationResult<Article>>(this.configService.configuration.api.article, obj);
  }
  deleteArticleById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.article + id);
  }
}
