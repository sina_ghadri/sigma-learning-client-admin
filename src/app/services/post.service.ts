import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult } from 'sigmasoft-ts/models';
import { Post }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class PostService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getPosts(): Observable<OperationResult<Post[]>> {
    return this.http.get<OperationResult<Post[]>>(this.configService.configuration.api.post).map(x => { x.data = Convert.toObject(x.data, Post); return x; });
  }
  getPostById(id: number): Observable<OperationResult<Post>> {
    return this.http.get<OperationResult<Post>>(this.configService.configuration.api.post + id).map(x => { x.data = Convert.toObject(x.data, Post); return x; });
  }
  insertPost(obj: Post): Observable<OperationResult<Post>> {
    return this.http.post<OperationResult<Post>>(this.configService.configuration.api.post, obj);
  }
  updatePost(obj: Post): Observable<OperationResult<Post>> {
    return this.http.patch<OperationResult<Post>>(this.configService.configuration.api.post, obj);
  }
  deletePostById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.post + id);
  }
}
