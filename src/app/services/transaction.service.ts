import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult } from 'sigmasoft-ts/models';
import { Transaction }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class TransactionService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getTransactions(): Observable<OperationResult<Transaction[]>> {
    return this.http.get<OperationResult<Transaction[]>>(this.configService.configuration.api.transaction).map(x => { x.data = Convert.toObject(x.data, Transaction); return x; });
  }
  getTransactionById(id: number): Observable<OperationResult<Transaction>> {
    return this.http.get<OperationResult<Transaction>>(this.configService.configuration.api.transaction + id).map(x => { x.data = Convert.toObject(x.data, Transaction); return x; });
  }
  insertTransaction(obj: Transaction): Observable<OperationResult<Transaction>> {
    return this.http.post<OperationResult<Transaction>>(this.configService.configuration.api.transaction, obj);
  }
  updateTransaction(obj: Transaction): Observable<OperationResult<Transaction>> {
    return this.http.patch<OperationResult<Transaction>>(this.configService.configuration.api.transaction, obj);
  }
  deleteTransactionById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.transaction + id);
  }
}
