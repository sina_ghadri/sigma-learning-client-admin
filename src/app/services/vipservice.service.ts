import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Convert } from 'sigmasoft-ts';
import { OperationResult, KeyValue } from 'sigmasoft-ts/models';
import { Vipservice }  from '../models';
import { ConfigurationService } from 'sigmasoft-ng';
import 'rxjs/add/operator/map';

@Injectable()
export class VipserviceService {

  constructor(private http: HttpClient, private configService: ConfigurationService) { }


  getStatuses(): KeyValue[] {
    return [
      { key: 0, value: 'عدم نمایش' },
      { key: 1, value: 'نمایش' },
    ]
  }
  getVipservices(): Observable<OperationResult<Vipservice[]>> {
    return this.http.get<OperationResult<Vipservice[]>>(this.configService.configuration.api.vipservice).map(x => { x.data = Convert.toObject(x.data, Vipservice); return x; });
  }
  getVipserviceById(id: number): Observable<OperationResult<Vipservice>> {
    return this.http.get<OperationResult<Vipservice>>(this.configService.configuration.api.vipservice + id).map(x => { x.data = Convert.toObject(x.data, Vipservice); return x; });
  }
  insertVipservice(obj: Vipservice): Observable<OperationResult<Vipservice>> {
    return this.http.post<OperationResult<Vipservice>>(this.configService.configuration.api.vipservice, obj);
  }
  updateVipservice(obj: Vipservice): Observable<OperationResult<Vipservice>> {
    return this.http.patch<OperationResult<Vipservice>>(this.configService.configuration.api.vipservice, obj);
  }
  deleteVipserviceById(id: number): Observable <OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.configService.configuration.api.vipservice + id);
  }
}
