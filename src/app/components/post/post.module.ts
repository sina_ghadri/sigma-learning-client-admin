import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { PostComponent } from './post.component';
import { FormsModule } from '@angular/forms';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { PostService, CategoryService } from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    CKEditorModule
  ],
  declarations: [PostComponent],
  exports: [PostComponent],
  providers: [PostService,CategoryService]
})
export class PostModule {}
