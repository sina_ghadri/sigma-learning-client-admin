import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { PostService, CategoryService } from '../../services';
import { Post, PostImage, Category } from '../../models';
import { Global } from 'sigmasoft-ts';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  @Input('post') obj: any = {};
  @Input() progress: boolean = false;
  @Output() oncomplete: EventEmitter<any> = new EventEmitter

  global: Global = Global
  posts: Post[] = [];
  categories: Category[];
  ckeditor = ClassicEditor;

  get images(): PostImage[] {
    if(this.obj.images) return this.obj.images;
    return this.obj.images = [];
  }

  constructor(private service: PostService, private categoryService: CategoryService) { }

  ngOnInit() {
    this.service.getPosts().subscribe(op => this.posts = op.data);
    this.categoryService.getCategorys().subscribe(op => this.categories = op.data);
  }
  compareWithId = Global.compareWithId;
  addLink(element: HTMLInputElement) {
    if (!element.value) return;
    if (!this.obj.links) this.obj.links = [];
    this.obj.links.push({ name: element.value, value: '' });
    element.value = '';
  }
  removeLink(item: any) {
    this.obj.links.splice(this.obj.links.indexOf(item), 1);
  }
  addTag(element: HTMLInputElement) {
    if (!element.value) return;
    if (!this.obj.tags) this.obj.tags = [];
    this.obj.tags.push(element.value);
    element.value = '';
  }
  removeTag(item: any) {
    this.obj.tags.splice(this.obj.tags.indexOf(item), 1);
  }
  addImage() {
    let tmp = new PostImage;
    this.images.push(tmp);
  }
  removeImage(item: any) {
    this.images.splice(this.obj.images.indexOf(item), 1);
  }
}
