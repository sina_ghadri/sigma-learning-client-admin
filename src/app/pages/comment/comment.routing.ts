import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommentIndexComponent } from './index/index.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: CommentIndexComponent }])],
  exports: [RouterModule]
})
export class CommentRoutingModule {}
