import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Comment } from "../../../models";
import { CommentService } from "../../../services";
import { Global } from "sigmasoft-ts";

@Component({
  selector: "page-comment-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class CommentEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;
  
  global = Global;
  obj: Comment = new Comment();

  @Output() oncomplete: EventEmitter<number> = new EventEmitter();

  constructor(private service: CommentService) {}
  ngOnInit(): void {
    this.service.getCommentById(this.id).subscribe(op => this.obj = op.data);
  }

  confirm() {
    this.progress = true;
    this.service.updateComment(this.obj).subscribe(
      op => {
        if (op.success) this.oncomplete.emit(1);
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
