import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommentService } from '../../../services';

@Component({
  selector: 'page-comment-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class CommentDeleteComponent implements OnInit {
  @Input() id: number;
    
  @Output() oncomplete: EventEmitter<number> = new EventEmitter;
  message: string;
  progress: boolean;
  
  constructor(private service: CommentService) {}
  ngOnInit(): void { }
  confirm() {
      this.progress = true;
      this.service.deleteCommentById(this.id).subscribe(op => {
          if(op.success)
              this.oncomplete.emit(1);
          else this.message = op.message;
      },e => this.progress = false,() => this.progress = false);
  }
  cancel() {
      this.oncomplete.emit(0);
  }
}
