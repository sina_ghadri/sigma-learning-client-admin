import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { CommentRoutingModule } from './comment.routing';
import { CommentIndexComponent } from './index/index.component';
import { CommentCreateComponent } from './create/create.component';
import { CommentDetailComponent } from './detail/detail.component';
import { CommentEditComponent } from './edit/edit.component';
import { CommentDeleteComponent } from './delete/delete.component';
import { CommentService } from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CommentRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    CommentIndexComponent,
    CommentCreateComponent,
    CommentDetailComponent,
    CommentEditComponent,
    CommentDeleteComponent,
  ],
  providers: [CommentService]
})
export class CommentModule { }
