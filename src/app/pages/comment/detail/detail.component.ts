import { Component, OnInit, Input } from "@angular/core";
import { CommentService } from "../../../services";
import { Comment } from "../../../models";

@Component({
  selector: "page-comment-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class CommentDetailComponent implements OnInit {
  @Input() id: number;

  obj: Comment;

  constructor(private service: CommentService) {}
  ngOnInit(): void {
    this.service.getCommentById(this.id).subscribe(op => this.obj = op.data);
  }
}
