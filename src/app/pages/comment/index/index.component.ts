import { Component, OnInit } from '@angular/core';
import {
    DataTable,
    DataTableButton as Button,
    DataTableColumn as Column,
    DataTableMenu as Menu,
    DataTableMenuItem as MenuItem,
    DataTableRow as Row,
    DataTableColumnSearchType as ColumnSearchType,
    Modal
} from "sigmasoft-ng";
import { CommentService } from '../../../services';
import { CommentStatus } from '../../../models';

@Component({
    selector: 'page-comment-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss'],
})
export class CommentIndexComponent implements OnInit {
    datatable: DataTable = new DataTable({ language: 'fa' });

    row: Row;
    insertModal: Modal = new Modal;
    detailModal: Modal = new Modal;
    updateModal: Modal = new Modal;
    deleteModal: Modal = new Modal;

    constructor(private service: CommentService) { }
    ngOnInit(): void {
        this.datatable.columns.push(new Column('شناسه', 'id').setWidth('60px'));
        this.datatable.columns.push(new Column('کاربر', row => row.value.user.name).setWidth('200px'));
        this.datatable.columns.push(new Column('متن', row => row.value.text.substr(0, 100)));
        this.datatable.columns.push(new Column('وضعیت', row => row.value.status.value).setWidth('60px').setSearchType(ColumnSearchType.Select));

        this.datatable.buttons.push(new Button('افزودن', () => this.insert(), 'mdi mdi-plus', 'btn-blue'));

        let menu = new Menu;
        menu.items.push(new MenuItem('تایید', row => this.approve(row), 'mdi mdi-check', 'btn-green').setIsEnable(x => x.value.status.key != CommentStatus.Approve));
        menu.items.push(new MenuItem('رد', row => this.reject(row), 'mdi mdi-close', 'btn-red').setIsEnable(x => x.value.status.key != CommentStatus.Reject));
        menu.items.push(new MenuItem('جزئیات', row => this.detail(row), 'mdi mdi-information-outline', 'btn-turquise'));
        menu.items.push(new MenuItem('ویرایش', row => this.update(row), 'mdi mdi-pencil', 'btn-blue'));
        menu.items.push(new MenuItem('حذف', row => this.delete(row), 'mdi mdi-delete', 'btn-red'));
        this.datatable.menus.push(menu);

        this.datatable.onrefresh.subscribe(() => this.load())
        this.load();
    }
    load() {
        this.datatable.progress = true;
        this.service.getComments().subscribe(op => this.datatable.dataSource = op.data, () => this.datatable.progress = false, () => this.datatable.progress = false);
    }
    loadById(id: number) {
        this.datatable.progress = true;
        this.service.getCommentById(id).subscribe(op => this.datatable.replaceRow(row => row.value.id == id, op.data), () => this.datatable.progress = false, () => this.datatable.progress = false);
    }
    insert() { this.insertModal.open(); }
    insertComplete(result: number) {
        if (result) this.load();
        this.insertModal.close();
    }
    detail(row: Row) { this.row = row; this.detailModal.open(); }
    approve(row: Row) {
        this.service.approveComment(row.value.id).subscribe(op => this.datatable.replaceRow(x => x.value.id == row.value.id, op.data));
    }
    reject(row: Row) {
        let reason = prompt('دلیل رد را وارد نمایید : ');
        if (reason != null) this.service.rejectComment(row.value.id, reason).subscribe(op => this.datatable.replaceRow(x => x.value.id == row.value.id, op.data));
    }
    update(row: Row) { this.row = row; this.updateModal.open(); }
    updateComplete(result: number) {
        if (result) this.loadById(this.row.value.id);
        this.updateModal.close();
    }
    delete(row: Row) { this.row = row; this.deleteModal.open(); }
    deleteComplete(result: number) {
        if (result) this.datatable.removeRow(row => row.value.id == this.row.value.id);
        this.deleteModal.close();
    }
}
