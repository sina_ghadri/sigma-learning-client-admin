import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CommentService } from '../../../services';
import { Comment } from '../../../models';

@Component({
	selector: 'page-comment-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class CommentCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: Comment = new Comment;

	@Output() oncomplete: EventEmitter<number> = new EventEmitter();

	constructor(private service: CommentService) { }
	ngOnInit(): void { }
	confirm() {
		this.progress = true;
		this.service.insertComment(this.obj).subscribe(
			op => {
				if (op.success) this.oncomplete.emit(1);
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.oncomplete.emit(0);
	}
}
