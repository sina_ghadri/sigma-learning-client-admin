import { Component, OnInit } from '@angular/core';
import {
    DataTable,
    DataTableButton as Button,
    DataTableColumn as Column,
    DataTableMenu as Menu,
    DataTableMenuItem as MenuItem,
    DataTableRow as Row,
    DataTableColumnSearchType as ColumnSearchType,
    Modal
} from "sigmasoft-ng";
import { VipserviceService } from '../../../services';
import { Formatter } from 'sigmasoft-ts';

@Component({
    selector: 'page-vipservice-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss'],
})
export class VipserviceIndexComponent implements OnInit {
    datatable: DataTable = new DataTable({ language: 'fa' });

    row: Row;
    insertModal: Modal = new Modal;
    detailModal: Modal = new Modal;
    updateModal: Modal = new Modal;
    deleteModal: Modal = new Modal;

    constructor(private service: VipserviceService) { }
    ngOnInit(): void {
        this.datatable.columns.push(new Column('شناسه', 'id').setWidth('60px'));
        this.datatable.columns.push(new Column('نام', 'name'));
        this.datatable.columns.push(new Column('نماد', 'symbol'));
        this.datatable.columns.push(new Column('قیمت (تومان)', 'price').setHtml(row => Formatter.seperate(row.value.price)));
        this.datatable.columns.push(new Column('مدت زمان (روز)', 'duration').setWidth('100px'));
        this.datatable.columns.push(new Column('وضعیت', row => row.value.status.value).setWidth('100px').setSearchType(ColumnSearchType.Select));

        this.datatable.buttons.push(new Button('افزودن', () => this.insert(), 'mdi mdi-plus', 'btn-blue'));

        let menu = new Menu;
        menu.items.push(new MenuItem('جزئیات', row => this.detail(row), 'mdi mdi-information-outline', 'btn-turquise'));
        menu.items.push(new MenuItem('ویرایش', row => this.update(row), 'mdi mdi-pencil', 'btn-blue'));
        menu.items.push(new MenuItem('حذف', row => this.delete(row), 'mdi mdi-delete', 'btn-red'));
        this.datatable.menus.push(menu);

        this.load();
    }
    load() {
        this.datatable.progress = true;
        this.service.getVipservices().subscribe(op => this.datatable.dataSource = op.data, () => this.datatable.progress = false, () => this.datatable.progress = false);
    }
    loadById(id: number) {
        this.datatable.progress = true;
        this.service.getVipserviceById(id).subscribe(op => this.datatable.replaceRow(row => row.value.id == id, op.data), () => this.datatable.progress = false, () => this.datatable.progress = false);
    }
    insert() { this.insertModal.open(); }
    insertComplete(result: number) {
        if (result) this.load();
        this.insertModal.close();
    }
    detail(row: Row) { this.row = row; this.detailModal.open(); }
    update(row: Row) { this.row = row; this.updateModal.open(); }
    updateComplete(result: number) {
        if (result) this.loadById(this.row.value.id);
        this.updateModal.close();
    }
    delete(row: Row) { this.row = row; this.deleteModal.open(); }
    deleteComplete(result: number) {
        if (result) this.datatable.removeRow(row => row.value.id == this.row.value.id);
        this.deleteModal.close();
    }
}
