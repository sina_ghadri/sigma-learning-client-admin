import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VipserviceIndexComponent } from './index/index.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: VipserviceIndexComponent }])],
  exports: [RouterModule]
})
export class VipserviceRoutingModule {}
