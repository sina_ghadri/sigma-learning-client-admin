import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Vipservice } from "../../../models";
import { VipserviceService } from "../../../services";
import { Global, KeyValue } from "sigmasoft-ts";

@Component({
  selector: "page-vipservice-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class VipserviceEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;
  
  global = Global;
  obj: Vipservice = new Vipservice();
	statuses: KeyValue[];

  @Output() oncomplete: EventEmitter<number> = new EventEmitter();

  constructor(private service: VipserviceService) {}
  ngOnInit(): void {
		this.statuses = this.service.getStatuses();
    this.service.getVipserviceById(this.id).subscribe(op => this.obj = op.data);
  }

  confirm() {
    this.progress = true;
    this.service.updateVipservice(this.obj).subscribe(
      op => {
        if (op.success) this.oncomplete.emit(1);
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
