import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { VipserviceRoutingModule } from './vipservice.routing';
import { VipserviceIndexComponent } from './index/index.component';
import { VipserviceCreateComponent } from './create/create.component';
import { VipserviceDetailComponent } from './detail/detail.component';
import { VipserviceEditComponent } from './edit/edit.component';
import { VipserviceDeleteComponent } from './delete/delete.component';
import { VipserviceService } from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    VipserviceRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    VipserviceIndexComponent,
    VipserviceCreateComponent,
    VipserviceDetailComponent,
    VipserviceEditComponent,
    VipserviceDeleteComponent,
  ],
  providers: [VipserviceService]
})
export class VipserviceModule { }
