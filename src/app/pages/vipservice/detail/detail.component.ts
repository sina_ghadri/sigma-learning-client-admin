import { Component, OnInit, Input } from "@angular/core";
import { VipserviceService } from "../../../services";
import { Vipservice } from "../../../models";

@Component({
  selector: "page-vipservice-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class VipserviceDetailComponent implements OnInit {
  @Input() id: number;

  obj: Vipservice;

  constructor(private service: VipserviceService) {}
  ngOnInit(): void {
    this.service.getVipserviceById(this.id).subscribe(op => this.obj = op.data);
  }
}
