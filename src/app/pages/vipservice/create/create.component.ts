import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { VipserviceService } from '../../../services';
import { Vipservice } from '../../../models';
import { KeyValue } from 'sigmasoft-ts';

@Component({
	selector: 'page-vipservice-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class VipserviceCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: Vipservice = new Vipservice;
	statuses: KeyValue[];

	@Output() oncomplete: EventEmitter<number> = new EventEmitter();

	constructor(private service: VipserviceService) { }
	ngOnInit(): void { 
		this.statuses = this.service.getStatuses();
	}
	confirm() {
		this.progress = true;
		this.service.insertVipservice(this.obj).subscribe(
			op => {
				if (op.success) this.oncomplete.emit(1);
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.oncomplete.emit(0);
	}
}
