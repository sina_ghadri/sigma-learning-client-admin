import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PostIndexComponent } from './index/index.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: PostIndexComponent }])],
  exports: [RouterModule]
})
export class PostRoutingModule {}
