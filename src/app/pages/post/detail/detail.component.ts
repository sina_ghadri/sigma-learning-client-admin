import { Component, OnInit, Input } from "@angular/core";
import { PostService } from "../../../services";
import { Post } from "../../../models";

@Component({
  selector: "page-post-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class PostDetailComponent implements OnInit {
  @Input() id: number;

  obj: Post;

  constructor(private service: PostService) {}
  ngOnInit(): void {
    this.service.getPostById(this.id).subscribe(op => this.obj = op.data);
  }
}
