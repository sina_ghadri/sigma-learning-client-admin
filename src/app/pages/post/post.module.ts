import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { PostRoutingModule } from './post.routing';
import { PostIndexComponent } from './index/index.component';
import { PostCreateComponent } from './create/create.component';
import { PostDetailComponent } from './detail/detail.component';
import { PostEditComponent } from './edit/edit.component';
import { PostDeleteComponent } from './delete/delete.component';
import { PostService } from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PostRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    PostIndexComponent,
    PostCreateComponent,
    PostDetailComponent,
    PostEditComponent,
    PostDeleteComponent,
  ],
  providers: [PostService]
})
export class PostModule { }
