import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PostService } from '../../../services';
import { Post } from '../../../models';

@Component({
	selector: 'page-post-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class PostCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: Post = new Post;

	@Output() oncomplete: EventEmitter<number> = new EventEmitter();

	constructor(private service: PostService) { }
	ngOnInit(): void { }
	confirm() {
		this.progress = true;
		this.service.insertPost(this.obj).subscribe(
			op => {
				if (op.success) this.oncomplete.emit(1);
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.oncomplete.emit(0);
	}
}
