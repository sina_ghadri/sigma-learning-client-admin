import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PostService } from '../../../services';

@Component({
  selector: 'page-post-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class PostDeleteComponent implements OnInit {
  @Input() id: number;
    
  @Output() oncomplete: EventEmitter<number> = new EventEmitter;
  message: string;
  progress: boolean;
  
  constructor(private service: PostService) {}
  ngOnInit(): void { }
  confirm() {
      this.progress = true;
      this.service.deletePostById(this.id).subscribe(op => {
          if(op.success)
              this.oncomplete.emit(1);
          else this.message = op.message;
      },e => this.progress = false,() => this.progress = false);
  }
  cancel() {
      this.oncomplete.emit(0);
  }
}
