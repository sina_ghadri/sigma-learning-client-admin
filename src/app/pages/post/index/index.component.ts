import { Component, OnInit } from '@angular/core';
import {
    DataTable,
    DataTableButton as Button,
    DataTableColumn as Column,
    DataTableMenu as Menu,
    DataTableMenuItem as MenuItem,
    DataTableRow as Row,
    DataTableColumnSearchType as ColumnSearchType,
    Modal
  } from "sigmasoft-ng";
import { PostService } from '../../../services';

@Component({
  selector: 'page-post-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class PostIndexComponent implements OnInit {
  datatable: DataTable = new DataTable({ language: 'fa' });
    
  row: Row;
  insertModal: Modal = new Modal;
  detailModal: Modal = new Modal;
  updateModal: Modal = new Modal;
  deleteModal: Modal = new Modal;

  constructor(private service: PostService) { }
  ngOnInit(): void {
      this.datatable.columns.push(new Column('شناسه', 'id').setWidth('60px'));
      this.datatable.columns.push(new Column('عنوان', row => row.value.title));
      this.datatable.columns.push(new Column('نماد', row => row.value.symbol));
      this.datatable.columns.push(new Column('زمان انتشار', row => row.value.publishTime));

      this.datatable.buttons.push(new Button('افزودن', () => this.insert(),'mdi mdi-plus','btn-blue'));

      let menu = new Menu;
      menu.items.push(new MenuItem('جزئیات', row => this.detail(row), 'mdi mdi-information-outline', 'btn-turquise'));
      menu.items.push(new MenuItem('ویرایش', row => this.update(row), 'mdi mdi-pencil', 'btn-blue'));
      menu.items.push(new MenuItem('حذف', row => this.delete(row), 'mdi mdi-delete', 'btn-red'));
      this.datatable.menus.push(menu);        
      
      this.load();
  }
  load() {
      this.datatable.progress = true;
      this.service.getPosts().subscribe(op => this.datatable.dataSource = op.data, () => this.datatable.progress = false, () => this.datatable.progress = false);
  }
  loadById(id: number) {
      this.datatable.progress = true;
      this.service.getPostById(id).subscribe(op => this.datatable.replaceRow(row => row.value.id == id, op.data), () => this.datatable.progress = false, () => this.datatable.progress = false);
  }
  insert() { this.insertModal.open(); }
  insertComplete(result: number) {
      if(result) this.load();
      this.insertModal.close();
  }
  detail(row: Row) { this.row = row; this.detailModal.open(); }
  update(row: Row) { this.row = row; this.updateModal.open(); }
  updateComplete(result: number) {
      if(result) this.loadById(this.row.value.id);
      this.updateModal.close();
  }
  delete(row: Row) { this.row = row; this.deleteModal.open(); }
  deleteComplete(result: number) {
      if(result) this.datatable.removeRow(row => row.value.id == this.row.value.id);
      this.deleteModal.close();
  }
}
