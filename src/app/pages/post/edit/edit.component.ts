import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Post } from "../../../models";
import { PostService } from "../../../services";
import { Global } from "sigmasoft-ts";

@Component({
  selector: "page-post-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class PostEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;
  
  global = Global;
  obj: Post = new Post();

  @Output() oncomplete: EventEmitter<number> = new EventEmitter();

  constructor(private service: PostService) {}
  ngOnInit(): void {
    this.service.getPostById(this.id).subscribe(op => this.obj = op.data);
  }

  confirm() {
    this.progress = true;
    this.service.updatePost(this.obj).subscribe(
      op => {
        if (op.success) this.oncomplete.emit(1);
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
