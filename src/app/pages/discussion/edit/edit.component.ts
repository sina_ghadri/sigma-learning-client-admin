import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Discussion } from "../../../models";
import { DiscussionService } from "../../../services";
import { Global } from "sigmasoft-ts";

@Component({
  selector: "page-discussion-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class DiscussionEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;
  
  global = Global;
  obj: Discussion = new Discussion();

  @Output() oncomplete: EventEmitter<number> = new EventEmitter();

  constructor(private service: DiscussionService) {}
  ngOnInit(): void {
    this.service.getDiscussionById(this.id).subscribe(op => this.obj = op.data);
  }

  confirm() {
    this.progress = true;
    this.service.updateDiscussion(this.obj).subscribe(
      op => {
        if (op.success) this.oncomplete.emit(1);
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
