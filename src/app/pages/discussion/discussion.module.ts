import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { DiscussionRoutingModule } from './discussion.routing';
import { DiscussionIndexComponent } from './index/index.component';
import { DiscussionCreateComponent } from './create/create.component';
import { DiscussionDetailComponent } from './detail/detail.component';
import { DiscussionEditComponent } from './edit/edit.component';
import { DiscussionDeleteComponent } from './delete/delete.component';
import { DiscussionService } from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DiscussionRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    DiscussionIndexComponent,
    DiscussionCreateComponent,
    DiscussionDetailComponent,
    DiscussionEditComponent,
    DiscussionDeleteComponent,
  ],
  providers: [DiscussionService]
})
export class DiscussionModule { }
