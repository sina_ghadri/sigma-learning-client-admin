import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DiscussionService } from '../../../services';
import { Discussion } from '../../../models';

@Component({
	selector: 'page-discussion-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class DiscussionCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: Discussion = new Discussion;

	@Output() oncomplete: EventEmitter<number> = new EventEmitter();

	constructor(private service: DiscussionService) { }
	ngOnInit(): void { }
	confirm() {
		this.progress = true;
		this.service.insertDiscussion(this.obj).subscribe(
			op => {
				if (op.success) this.oncomplete.emit(1);
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.oncomplete.emit(0);
	}
}
