import { Component, OnInit, Input } from "@angular/core";
import { DiscussionService } from "../../../services";
import { Discussion } from "../../../models";

@Component({
  selector: "page-discussion-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class DiscussionDetailComponent implements OnInit {
  @Input() id: number;

  obj: Discussion;

  constructor(private service: DiscussionService) {}
  ngOnInit(): void {
    this.service.getDiscussionById(this.id).subscribe(op => this.obj = op.data);
  }
}
