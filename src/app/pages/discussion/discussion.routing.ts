import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DiscussionIndexComponent } from './index/index.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: DiscussionIndexComponent }])],
  exports: [RouterModule]
})
export class DiscussionRoutingModule {}
