import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LessonIndexComponent } from './index/index.component';
import { LessonDetailComponent } from './detail/detail.component';
import { LessonCreateComponent } from './create/create.component';
import { LessonEditComponent } from './edit/edit.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: '', component: LessonIndexComponent },
    { path: 'create', component: LessonCreateComponent },
    { path: 'create/:id', component: LessonCreateComponent },
    { path: 'detail/:id', component: LessonDetailComponent },
    { path: 'edit/:id', component: LessonEditComponent },
    { path: ':id', component: LessonIndexComponent },
  ])],
  exports: [RouterModule]
})
export class LessonRoutingModule {}
