import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { LessonRoutingModule } from './lesson.routing';
import { LessonIndexComponent } from './index/index.component';
import { LessonCreateComponent } from './create/create.component';
import { LessonDetailComponent } from './detail/detail.component';
import { LessonEditComponent } from './edit/edit.component';
import { LessonDeleteComponent } from './delete/delete.component';
import { LessonService } from '../../services';
import { PostModule } from '../../components/post/post.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PostModule,
    LessonRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    LessonIndexComponent,
    LessonCreateComponent,
    LessonDetailComponent,
    LessonEditComponent,
    LessonDeleteComponent,
  ],
  providers: [LessonService]
})
export class LessonModule { }
