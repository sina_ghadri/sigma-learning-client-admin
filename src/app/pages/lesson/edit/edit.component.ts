import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Lesson, Course } from "../../../models";
import { LessonService, CourseService } from "../../../services";
import { Global } from "sigmasoft-ts";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

@Component({
  selector: "page-lesson-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"],
  providers: [CourseService]
})
export class LessonEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;

  global = Global;
  obj: Lesson = new Lesson();
  courseId: number;
  courses: Course[];

  constructor(private service: LessonService, private courseService: CourseService, private route: ActivatedRoute, private location: Location) { }
  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.service.getLessonById(this.courseId = +param['id']).subscribe(op => this.obj = op.data);
    });
		this.courseService.getCourses().subscribe(op => this.courses = op.data);
  }
  courseCompare() { return (x1,x2) => x2 && x2.id == x1.id; }
  courseText() { return item => item.post.title; }
  complete(result: number) {
    if (result) this.confirm();
    else this.cancel();
  }
  confirm() {
    this.progress = true;
    this.service.updateLesson(this.obj).subscribe(
      op => {
        if (op.success) this.location.back();
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.location.back();
  }
}
