import { Component, OnInit, Input } from "@angular/core";
import { LessonService } from "../../../services";
import { Lesson } from "../../../models";

@Component({
  selector: "page-lesson-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class LessonDetailComponent implements OnInit {
  @Input() id: number;

  obj: Lesson;

  constructor(private service: LessonService) {}
  ngOnInit(): void {
    this.service.getLessonById(this.id).subscribe(op => this.obj = op.data);
  }
}
