import { Component, OnInit } from '@angular/core';
import {
    DataTable,
    DataTableButton as Button,
    DataTableColumn as Column,
    DataTableMenu as Menu,
    DataTableMenuItem as MenuItem,
    DataTableRow as Row,
    DataTableColumnSearchType as ColumnSearchType,
    Modal
} from "sigmasoft-ng";
import { LessonService, CourseService } from '../../../services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'page-lesson-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss'],
    providers: [CourseService]
})
export class LessonIndexComponent implements OnInit {
    datatable: DataTable = new DataTable({ language: 'fa' });

    row: Row;
    course: any;
    deleteModal: Modal = new Modal;

    constructor(private service: LessonService, private courseService: CourseService, private router: Router, private route: ActivatedRoute) { }
    ngOnInit(): void {
        this.datatable.columns.push(new Column('شناسه', 'id').setWidth('60px'));
        this.datatable.columns.push(new Column('عنوان', row => row.value.post.title));
        this.datatable.columns.push(new Column('نماد', row => row.value.post.symbol));
        this.datatable.columns.push(new Column('زمان انتشار', row => row.value.post.publishTime).setWidth('100px'));
        this.datatable.columns.push(new Column('دوره', row => row.value.course.post.title).setSearchType(ColumnSearchType.Select));
        this.datatable.columns.push(new Column('مدرس', row => row.value.course.teacher.name).setSearchType(ColumnSearchType.Select));

        this.datatable.buttons.push(new Button('افزودن', () => this.insert(), 'mdi mdi-plus', 'btn-blue'));

        let menu = new Menu;
        menu.items.push(new MenuItem('جزئیات', row => this.detail(row), 'mdi mdi-information-outline', 'btn-turquise'));
        menu.items.push(new MenuItem('ویرایش', row => this.update(row), 'mdi mdi-pencil', 'btn-blue'));
        menu.items.push(new MenuItem('حذف', row => this.delete(row), 'mdi mdi-delete', 'btn-red'));
        this.datatable.menus.push(menu);

        this.load();
    }
    load() {
        this.route.params.subscribe(params => {
            this.datatable.progress = true;
            if (params['id']) {
                let id = +params['id'];
                this.courseService.getCourseById(id).subscribe(op => this.course = op.data);
                this.courseService.getLessons(id).subscribe(op => this.datatable.dataSource = op.data, () => this.datatable.progress = false, () => this.datatable.progress = false);
            }
            else this.service.getLessons().subscribe(op => this.datatable.dataSource = op.data, () => this.datatable.progress = false, () => this.datatable.progress = false);
        })

    }
    loadById(id: number) {
        this.datatable.progress = true;
        this.service.getLessonById(id).subscribe(op => this.datatable.replaceRow(row => row.value.id == id, op.data), () => this.datatable.progress = false, () => this.datatable.progress = false);
    }
    insert() {
        if (this.course) this.router.navigate(['lesson', 'create', this.course.id]);
        else this.router.navigate(['lesson', 'create']);
    }
    detail(row: Row) { this.router.navigate(['lesson', 'detail', row.value.id]); }
    update(row: Row) { this.router.navigate(['lesson', 'edit', row.value.id]); }
    delete(row: Row) { this.row = row; this.deleteModal.open(); }
    deleteComplete(result: number) {
        if (result) this.datatable.removeRow(row => row.value.id == this.row.value.id);
        this.deleteModal.close();
    }
}
