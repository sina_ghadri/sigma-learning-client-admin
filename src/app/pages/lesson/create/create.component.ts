import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LessonService, CourseService } from '../../../services';
import { Lesson, Course } from '../../../models';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'page-lesson-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss'],
	providers: [CourseService]
})
export class LessonCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	courseId: number;
	obj: Lesson = new Lesson;
	courses: Course[] = [];

	constructor(private service: LessonService, private courseService: CourseService,private route: ActivatedRoute, private location: Location) { }
	ngOnInit(): void { 
		this.route.params.subscribe(params => {
			if(params['id']) this.courseId = this.obj.course.id = +params['id'];
		});
		this.courseService.getCourses().subscribe(op => this.courses = op.data);
	}
	courseText() { return item => item.post.title; }
	complete(result: number) {
		if (result) this.confirm();
		else this.cancel();
	}
	confirm() {
		this.progress = true;
		this.service.insertLesson(this.obj).subscribe(
			op => {
				if (op.success) this.location.back();
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.location.back();
	}
}
