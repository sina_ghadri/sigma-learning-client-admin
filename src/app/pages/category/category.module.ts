import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { CategoryRoutingModule } from './category.routing';
import { CategoryIndexComponent } from './index/index.component';
import { CategoryCreateComponent } from './create/create.component';
import { CategoryDetailComponent } from './detail/detail.component';
import { CategoryEditComponent } from './edit/edit.component';
import { CategoryDeleteComponent } from './delete/delete.component';
import { CategoryService } from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CategoryRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    CategoryIndexComponent,
    CategoryCreateComponent,
    CategoryDetailComponent,
    CategoryEditComponent,
    CategoryDeleteComponent,
  ],
  providers: [CategoryService]
})
export class CategoryModule { }
