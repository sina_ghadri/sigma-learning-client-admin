import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CategoryIndexComponent } from './index/index.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: CategoryIndexComponent }])],
  exports: [RouterModule]
})
export class CategoryRoutingModule {}
