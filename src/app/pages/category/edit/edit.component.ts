import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Category } from "../../../models";
import { CategoryService } from "../../../services";
import { Global } from "sigmasoft-ts";

@Component({
  selector: "page-category-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class CategoryEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;
  
  global = Global;
  obj: Category = new Category();
	parents: Category[];

  @Output() oncomplete: EventEmitter<number> = new EventEmitter();

  constructor(private service: CategoryService) {}
  ngOnInit(): void {
    this.service.getCategoryById(this.id).subscribe(op => this.obj = op.data);
		this.service.getCategorys().subscribe(op => this.parents = op.data);
  }

  confirm() {
    this.progress = true;
    this.service.updateCategory(this.obj).subscribe(
      op => {
        if (op.success) this.oncomplete.emit(1);
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
