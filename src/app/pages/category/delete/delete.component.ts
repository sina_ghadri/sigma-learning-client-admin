import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CategoryService } from '../../../services';

@Component({
  selector: 'page-category-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class CategoryDeleteComponent implements OnInit {
  @Input() id: number;
    
  @Output() oncomplete: EventEmitter<number> = new EventEmitter;
  message: string;
  progress: boolean;
  
  constructor(private service: CategoryService) {}
  ngOnInit(): void { }
  confirm() {
      this.progress = true;
      this.service.deleteCategoryById(this.id).subscribe(op => {
          if(op.success)
              this.oncomplete.emit(1);
          else this.message = op.message;
      },e => this.progress = false,() => this.progress = false);
  }
  cancel() {
      this.oncomplete.emit(0);
  }
}
