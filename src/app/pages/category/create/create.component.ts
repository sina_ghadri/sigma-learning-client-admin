import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CategoryService } from '../../../services';
import { Category } from '../../../models';

@Component({
	selector: 'page-category-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class CategoryCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: Category = new Category;
	parents: Category[];

	@Output() oncomplete: EventEmitter<number> = new EventEmitter();

	constructor(private service: CategoryService) { }
	ngOnInit(): void { 
		this.service.getCategorys().subscribe(op => this.parents = op.data);
	}
	confirm() {
		this.progress = true;
		this.service.insertCategory(this.obj).subscribe(
			op => {
				if (op.success) this.oncomplete.emit(1);
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.oncomplete.emit(0);
	}
}
