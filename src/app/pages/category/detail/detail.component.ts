import { Component, OnInit, Input } from "@angular/core";
import { CategoryService } from "../../../services";
import { Category } from "../../../models";

@Component({
  selector: "page-category-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class CategoryDetailComponent implements OnInit {
  @Input() id: number;

  obj: Category;

  constructor(private service: CategoryService) {}
  ngOnInit(): void {
    this.service.getCategoryById(this.id).subscribe(op => this.obj = op.data);
  }
}
