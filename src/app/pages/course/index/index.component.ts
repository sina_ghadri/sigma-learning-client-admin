import { Component, OnInit } from '@angular/core';
import {
    DataTable,
    DataTableButton as Button,
    DataTableColumn as Column,
    DataTableMenu as Menu,
    DataTableMenuItem as MenuItem,
    DataTableRow as Row,
    DataTableColumnSearchType as ColumnSearchType,
    Modal
} from "sigmasoft-ng";
import { CourseService } from '../../../services';
import { Router, ActivatedRoute } from '@angular/router';
import { Formatter } from 'sigmasoft-ts';

@Component({
    selector: 'page-course-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss'],
})
export class CourseIndexComponent implements OnInit {
    datatable: DataTable = new DataTable({ language: 'fa' });

    row: Row;
    deleteModal: Modal = new Modal;

    constructor(private service: CourseService, private router: Router, private route: ActivatedRoute) { }
    ngOnInit(): void {
        this.datatable.columns.push(new Column('شناسه', 'id').setWidth('60px'));
        this.datatable.columns.push(new Column('عنوان', row => row.value.post.title));
        this.datatable.columns.push(new Column('نماد', row => row.value.post.symbol));
        this.datatable.columns.push(new Column('مدرس', row => row.value.teacher.name));
        this.datatable.columns.push(new Column('زمان انتشار', row => row.value.post.publishTime).setWidth('100px'));
        this.datatable.columns.push(new Column('قیمت (تومان)', row => row.value.price).setHtml(row => Formatter.seperate(row.value.price)).setWidth('90px'));
        this.datatable.columns.push(new Column('نوع', row => row.value.type.value).setSearchType(ColumnSearchType.Select).setWidth('90px'));
        this.datatable.columns.push(new Column('وضعیت', row => row.value.status.value).setSearchType(ColumnSearchType.Select).setWidth('90px'));

        this.datatable.buttons.push(new Button('افزودن', () => this.insert(), 'mdi mdi-plus', 'btn-blue'));

        let menu = new Menu;
        menu.items.push(new MenuItem('دروس', row => this.lessons(row), 'mdi mdi-dots-vertical', 'btn-violet'));
        menu.items.push(new MenuItem('جزئیات', row => this.detail(row), 'mdi mdi-information-outline', 'btn-turquise'));
        menu.items.push(new MenuItem('ویرایش', row => this.update(row), 'mdi mdi-pencil', 'btn-blue'));
        menu.items.push(new MenuItem('حذف', row => this.delete(row), 'mdi mdi-delete', 'btn-red'));
        this.datatable.menus.push(menu);

        this.load();
    }
    load() {
        this.datatable.progress = true;
        this.service.getCourses().subscribe(op => this.datatable.dataSource = op.data, () => this.datatable.progress = false, () => this.datatable.progress = false);
    }
    loadById(id: number) {
        this.datatable.progress = true;
        this.service.getCourseById(id).subscribe(op => this.datatable.replaceRow(row => row.value.id == id, op.data), () => this.datatable.progress = false, () => this.datatable.progress = false);
    }
    insert() { this.router.navigate(['create'], { relativeTo: this.route }); }
    lessons(row: Row) { this.router.navigate(['lesson', row.value.id]); }
    detail(row: Row) { this.router.navigate(['detail', row.value.id], { relativeTo: this.route }); }
    update(row: Row) { this.router.navigate(['edit', row.value.id], { relativeTo: this.route }); }
    delete(row: Row) { this.row = row; this.deleteModal.open(); }
    deleteComplete(result: number) {
        if (result) this.datatable.removeRow(row => row.value.id == this.row.value.id);
        this.deleteModal.close();
    }
}
