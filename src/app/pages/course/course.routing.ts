import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CourseIndexComponent } from './index/index.component';
import { CourseCreateComponent } from './create/create.component';
import { CourseEditComponent } from './edit/edit.component';
import { CourseDetailComponent } from './detail/detail.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: '', component: CourseIndexComponent },
    { path: 'create', component: CourseCreateComponent },
    { path: 'edit/:id', component: CourseEditComponent },
    { path: 'detail/:id', component: CourseDetailComponent },
  ])],
  exports: [RouterModule]
})
export class CourseRoutingModule {}
