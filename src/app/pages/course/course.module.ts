import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { CourseRoutingModule } from './course.routing';
import { CourseIndexComponent } from './index/index.component';
import { CourseCreateComponent } from './create/create.component';
import { CourseDetailComponent } from './detail/detail.component';
import { CourseEditComponent } from './edit/edit.component';
import { CourseDeleteComponent } from './delete/delete.component';
import { CourseService } from '../../services';
import { PostModule } from '../../components/post/post.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PostModule,
    CourseRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    CourseIndexComponent,
    CourseCreateComponent,
    CourseDetailComponent,
    CourseEditComponent,
    CourseDeleteComponent,
  ],
  providers: [CourseService]
})
export class CourseModule { }
