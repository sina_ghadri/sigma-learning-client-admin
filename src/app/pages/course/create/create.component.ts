import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { CourseService, UserService } from '../../../services';
import { Course, User } from '../../../models';
import { KeyValue } from 'sigmasoft-ts';

@Component({
	selector: 'page-course-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss'],
	providers: [UserService]
})
export class CourseCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: Course = new Course;
	teachers: User[];
	types: KeyValue[];
	statuses: KeyValue[];

	@Output() oncomplete: EventEmitter<number> = new EventEmitter();

	constructor(private service: CourseService, private userService: UserService, private location: Location) { }
	ngOnInit(): void { 
		this.types = this.service.getTypes();
		this.statuses = this.service.getStatuses();
		this.userService.getUsers().subscribe(op => this.teachers = op.data);
	}
	complete(result: number) {
		if (result) this.confirm();
		else this.cancel();
	}
	confirm() {
		this.progress = true;
		this.service.insertCourse(this.obj).subscribe(
			op => {
				if (op.success) this.location.back();
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.location.back();
	}
}
