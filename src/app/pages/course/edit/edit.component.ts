import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Course, User } from "../../../models";
import { CourseService, UserService } from "../../../services";
import { Global, KeyValue } from "sigmasoft-ts";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "page-course-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"],
  providers: [UserService]
})
export class CourseEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;

  global = Global;
  obj: Course = new Course();
  teachers: User[];
	types: KeyValue[];
	statuses: KeyValue[];

  constructor(private service: CourseService, private userService:UserService, private route: ActivatedRoute, private location: Location) { }
  ngOnInit(): void {
    this.route.params.subscribe(param => this.service.getCourseById(+param['id']).subscribe(op => this.obj = op.data));
    this.types = this.service.getTypes();
    this.statuses = this.service.getStatuses();
    this.userService.getUsers().subscribe(op => this.teachers = op.data)
  }
  complete(result: number) {
    if (result) this.confirm();
    else this.cancel();
  }
  confirm() {
    this.progress = true;
    this.service.updateCourse(this.obj).subscribe(
      op => {
        if (op.success) this.location.back();
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.location.back();
  }
}
