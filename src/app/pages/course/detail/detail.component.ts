import { Component, OnInit, Input } from "@angular/core";
import { CourseService } from "../../../services";
import { Course } from "../../../models";

@Component({
  selector: "page-course-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class CourseDetailComponent implements OnInit {
  @Input() id: number;

  obj: Course;

  constructor(private service: CourseService) {}
  ngOnInit(): void {
    this.service.getCourseById(this.id).subscribe(op => this.obj = op.data);
  }
}
