import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Media } from "../../../models";
import { MediaService } from "../../../services";
import { Global } from "sigmasoft-ts";

@Component({
  selector: "page-media-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class MediaEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;
  
  global = Global;
  obj: Media = new Media();

  @Output() oncomplete: EventEmitter<number> = new EventEmitter();

  constructor(private service: MediaService) {}
  ngOnInit(): void {
    this.service.getMediaById(this.id).subscribe(op => this.obj = op.data);
  }

  confirm() {
    this.progress = true;
    this.service.updateMedia(this.obj).subscribe(
      op => {
        if (op.success) this.oncomplete.emit(1);
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
