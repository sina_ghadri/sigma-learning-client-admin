import { Component, OnInit, Input } from "@angular/core";
import { MediaService } from "../../../services";
import { Media } from "../../../models";

@Component({
  selector: "page-media-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class MediaDetailComponent implements OnInit {
  @Input() id: number;

  obj: Media;

  constructor(private service: MediaService) {}
  ngOnInit(): void {
    this.service.getMediaById(this.id).subscribe(op => this.obj = op.data);
  }
}
