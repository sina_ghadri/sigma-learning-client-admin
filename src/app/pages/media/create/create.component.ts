import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MediaService } from '../../../services';
import { Media } from '../../../models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
	selector: 'page-media-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class MediaCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: Media = new Media;
	form: FormGroup;

	@Output() oncomplete: EventEmitter<number> = new EventEmitter();

	constructor(private service: MediaService, private fb: FormBuilder) { }
	ngOnInit(): void {
		this.form = this.fb.group({
			name: ['', Validators.required],
			avatar: null
		});
	}

	onFileChange(event) {
		let reader = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			let file = event.target.files[0];
			reader.readAsDataURL(file);
			reader.onload = () => {
				this.form.get('avatar').setValue({
					filename: file.name,
					filetype: file.type,
					value: reader.result.toString().split(',')[1]
				})
			};
		}
	}

	confirm() {
		this.progress = true;
		this.service.insertMedia(this.obj).subscribe(
			op => {
				if (op.success) this.oncomplete.emit(1);
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.oncomplete.emit(0);
	}
}
