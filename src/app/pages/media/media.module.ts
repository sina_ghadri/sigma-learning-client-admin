import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MediaRoutingModule } from './media.routing';
import { MediaIndexComponent } from './index/index.component';
import { MediaCreateComponent } from './create/create.component';
import { MediaDetailComponent } from './detail/detail.component';
import { MediaEditComponent } from './edit/edit.component';
import { MediaDeleteComponent } from './delete/delete.component';
import { MediaService } from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MediaRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    MediaIndexComponent,
    MediaCreateComponent,
    MediaDetailComponent,
    MediaEditComponent,
    MediaDeleteComponent,
  ],
  providers: [MediaService]
})
export class MediaModule { }
