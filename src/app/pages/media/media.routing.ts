import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MediaIndexComponent } from './index/index.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: MediaIndexComponent }])],
  exports: [RouterModule]
})
export class MediaRoutingModule {}
