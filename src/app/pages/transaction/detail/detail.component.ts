import { Component, OnInit, Input } from "@angular/core";
import { TransactionService } from "../../../services";
import { Transaction } from "../../../models";

@Component({
  selector: "page-transaction-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class TransactionDetailComponent implements OnInit {
  @Input() id: number;

  obj: Transaction;

  constructor(private service: TransactionService) {}
  ngOnInit(): void {
    this.service.getTransactionById(this.id).subscribe(op => this.obj = op.data);
  }
}
