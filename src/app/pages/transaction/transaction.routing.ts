import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TransactionIndexComponent } from './index/index.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: TransactionIndexComponent }])],
  exports: [RouterModule]
})
export class TransactionRoutingModule {}
