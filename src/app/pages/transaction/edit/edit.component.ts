import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Transaction } from "../../../models";
import { TransactionService } from "../../../services";
import { Global } from "sigmasoft-ts";

@Component({
  selector: "page-transaction-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class TransactionEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;
  
  global = Global;
  obj: Transaction = new Transaction();

  @Output() oncomplete: EventEmitter<number> = new EventEmitter();

  constructor(private service: TransactionService) {}
  ngOnInit(): void {
    this.service.getTransactionById(this.id).subscribe(op => this.obj = op.data);
  }

  confirm() {
    this.progress = true;
    this.service.updateTransaction(this.obj).subscribe(
      op => {
        if (op.success) this.oncomplete.emit(1);
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
