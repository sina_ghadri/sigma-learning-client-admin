import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { TransactionRoutingModule } from './transaction.routing';
import { TransactionIndexComponent } from './index/index.component';
import { TransactionCreateComponent } from './create/create.component';
import { TransactionDetailComponent } from './detail/detail.component';
import { TransactionEditComponent } from './edit/edit.component';
import { TransactionDeleteComponent } from './delete/delete.component';
import { TransactionService } from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TransactionRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    TransactionIndexComponent,
    TransactionCreateComponent,
    TransactionDetailComponent,
    TransactionEditComponent,
    TransactionDeleteComponent,
  ],
  providers: [TransactionService]
})
export class TransactionModule { }
