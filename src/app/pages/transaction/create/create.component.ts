import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TransactionService } from '../../../services';
import { Transaction } from '../../../models';

@Component({
	selector: 'page-transaction-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class TransactionCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: Transaction = new Transaction;

	@Output() oncomplete: EventEmitter<number> = new EventEmitter();

	constructor(private service: TransactionService) { }
	ngOnInit(): void { }
	confirm() {
		this.progress = true;
		this.service.insertTransaction(this.obj).subscribe(
			op => {
				if (op.success) this.oncomplete.emit(1);
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.oncomplete.emit(0);
	}
}
