import { Component, OnInit, Input } from "@angular/core";
import { ArticleService } from "../../../services";
import { Article } from "../../../models";

@Component({
  selector: "page-article-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class ArticleDetailComponent implements OnInit {
  @Input() id: number;

  obj: Article;

  constructor(private service: ArticleService) {}
  ngOnInit(): void {
    this.service.getArticleById(this.id).subscribe(op => this.obj = op.data);
  }
}
