import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ArticleService } from '../../../services';
import { Article } from '../../../models';
import { Route } from '@angular/router';
import { Location } from '@angular/common';

@Component({
	selector: 'page-article-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class ArticleCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: Article = new Article;


	constructor(private service: ArticleService, private location: Location) { }
	ngOnInit(): void { }
	complete(result: number) {
		if(result) this.confirm();
		else this.cancel();
	}
	confirm() {
		this.progress = true;
		this.service.insertArticle(this.obj).subscribe(
			op => {
				if (op.success) this.location.back();
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() { this.location.back(); }
}
