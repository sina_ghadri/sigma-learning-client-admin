import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ArticleIndexComponent } from './index/index.component';
import { ArticleCreateComponent } from './create/create.component';
import { ArticleEditComponent } from './edit/edit.component';
import { ArticleDetailComponent } from './detail/detail.component';
import { ArticleDeleteComponent } from './delete/delete.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: '', component: ArticleIndexComponent },
    { path: 'create', component: ArticleCreateComponent },
    { path: 'edit/:id', component: ArticleEditComponent },
    { path: 'detail/:id', component: ArticleDetailComponent },
  ])],
  exports: [RouterModule]
})
export class ArticleRoutingModule {}
