import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { ArticleRoutingModule } from './article.routing';
import { ArticleIndexComponent } from './index/index.component';
import { ArticleCreateComponent } from './create/create.component';
import { ArticleDetailComponent } from './detail/detail.component';
import { ArticleEditComponent } from './edit/edit.component';
import { ArticleDeleteComponent } from './delete/delete.component';
import { ArticleService } from '../../services';
import { PostModule } from '../../components/post/post.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PostModule,
    ArticleRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    ArticleIndexComponent,
    ArticleCreateComponent,
    ArticleDetailComponent,
    ArticleEditComponent,
    ArticleDeleteComponent,
  ],
  providers: [ArticleService]
})
export class ArticleModule { }
