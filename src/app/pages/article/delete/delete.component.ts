import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ArticleService } from '../../../services';

@Component({
  selector: 'page-article-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class ArticleDeleteComponent implements OnInit {
  @Input() id: number;
    
  @Output() oncomplete: EventEmitter<number> = new EventEmitter;
  message: string;
  progress: boolean;
  
  constructor(private service: ArticleService) {}
  ngOnInit(): void { }
  confirm() {
      this.progress = true;
      this.service.deleteArticleById(this.id).subscribe(op => {
          if(op.success)
              this.oncomplete.emit(1);
          else this.message = op.message;
      },e => this.progress = false,() => this.progress = false);
  }
  cancel() {
      this.oncomplete.emit(0);
  }
}
