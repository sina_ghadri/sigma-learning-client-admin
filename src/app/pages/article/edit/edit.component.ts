import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Article } from "../../../models";
import { ArticleService } from "../../../services";
import { Global } from "sigmasoft-ts";
import { Location } from "@angular/common";

@Component({
  selector: "page-article-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class ArticleEditComponent implements OnInit {
  progress: boolean;
  message: string;

  global = Global;
  obj: Article = new Article();

  @Output() oncomplete: EventEmitter<number> = new EventEmitter();

  constructor(private service: ArticleService, private route: ActivatedRoute, private location: Location) { }
  ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.service.getArticleById(+param['id']).subscribe(op => this.obj = op.data);
    });
  }
  complete(result: number) {
    if (result) this.confirm();
    else this.cancel();
  }
  confirm() {
    this.progress = true;
    this.service.updateArticle(this.obj).subscribe(
      op => {
        if (op.success) this.location.back();
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.location.back();
  }
}
