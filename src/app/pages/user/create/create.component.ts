import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../../services';
import { User } from '../../../models';

@Component({
	selector: 'page-user-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class UserCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: User = new User;

	@Output() oncomplete: EventEmitter<number> = new EventEmitter();

	constructor(private service: UserService) { }
	ngOnInit(): void { }
	confirm() {
		this.progress = true;
		this.service.insertUser(this.obj).subscribe(
			op => {
				if (op.success) this.oncomplete.emit(1);
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.oncomplete.emit(0);
	}
}
