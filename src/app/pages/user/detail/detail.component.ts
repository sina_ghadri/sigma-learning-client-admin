import { Component, OnInit, Input } from "@angular/core";
import { UserService } from "../../../services";
import { User } from "../../../models";

@Component({
  selector: "page-user-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class UserDetailComponent implements OnInit {
  @Input() id: number;

  obj: User;

  constructor(private service: UserService) {}
  ngOnInit(): void {
    this.service.getUserById(this.id).subscribe(op => this.obj = op.data);
  }
}
