import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { User } from "../../../models";
import { UserService } from "../../../services";
import { Global } from "sigmasoft-ts";

@Component({
  selector: "page-user-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class UserEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;
  
  global = Global;
  obj: User = new User();

  @Output() oncomplete: EventEmitter<number> = new EventEmitter();

  constructor(private service: UserService) {}
  ngOnInit(): void {
    this.service.getUserById(this.id).subscribe(op => this.obj = op.data);
  }

  confirm() {
    this.progress = true;
    this.service.updateUser(this.obj).subscribe(
      op => {
        if (op.success) this.oncomplete.emit(1);
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
