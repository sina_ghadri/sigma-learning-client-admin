import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UserIndexComponent } from './index/index.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: UserIndexComponent }])],
  exports: [RouterModule]
})
export class UserRoutingModule {}
