import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { UserRoutingModule } from './user.routing';
import { UserIndexComponent } from './index/index.component';
import { UserCreateComponent } from './create/create.component';
import { UserDetailComponent } from './detail/detail.component';
import { UserEditComponent } from './edit/edit.component';
import { UserDeleteComponent } from './delete/delete.component';
import { UserService } from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UserRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    UserIndexComponent,
    UserCreateComponent,
    UserDetailComponent,
    UserEditComponent,
    UserDeleteComponent,
  ],
  providers: [UserService]
})
export class UserModule { }
