import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MessageIndexComponent } from './index/index.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: MessageIndexComponent }])],
  exports: [RouterModule]
})
export class MessageRoutingModule {}
