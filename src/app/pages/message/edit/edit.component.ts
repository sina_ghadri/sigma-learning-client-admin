import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { Message } from "../../../models";
import { MessageService } from "../../../services";
import { Global } from "sigmasoft-ts";

@Component({
  selector: "page-message-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"]
})
export class MessageEditComponent implements OnInit {
  @Input() id: number;
  progress: boolean;
  message: string;
  
  global = Global;
  obj: Message = new Message();

  @Output() oncomplete: EventEmitter<number> = new EventEmitter();

  constructor(private service: MessageService) {}
  ngOnInit(): void {
    this.service.getMessageById(this.id).subscribe(op => this.obj = op.data);
  }

  confirm() {
    this.progress = true;
    this.service.updateMessage(this.obj).subscribe(
      op => {
        if (op.success) this.oncomplete.emit(1);
        else this.message = op.message;
      },
      e => (this.progress = false),
      () => (this.progress = false)
    );
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
