import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MessageService } from '../../../services';

@Component({
  selector: 'page-message-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class MessageDeleteComponent implements OnInit {
  @Input() id: number;
    
  @Output() oncomplete: EventEmitter<number> = new EventEmitter;
  message: string;
  progress: boolean;
  
  constructor(private service: MessageService) {}
  ngOnInit(): void { }
  confirm() {
      this.progress = true;
      this.service.deleteMessageById(this.id).subscribe(op => {
          if(op.success)
              this.oncomplete.emit(1);
          else this.message = op.message;
      },e => this.progress = false,() => this.progress = false);
  }
  cancel() {
      this.oncomplete.emit(0);
  }
}
