import { Component, OnInit, Input } from "@angular/core";
import { MessageService } from "../../../services";
import { Message } from "../../../models";

@Component({
  selector: "page-message-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class MessageDetailComponent implements OnInit {
  @Input() id: number;

  obj: Message;

  constructor(private service: MessageService) {}
  ngOnInit(): void {
    this.service.getMessageById(this.id).subscribe(op => this.obj = op.data);
  }
}
