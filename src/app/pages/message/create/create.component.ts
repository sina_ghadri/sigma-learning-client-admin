import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MessageService } from '../../../services';
import { Message } from '../../../models';

@Component({
	selector: 'page-message-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class MessageCreateComponent implements OnInit {
	progress: boolean;
	message: string;

	obj: Message = new Message;

	@Output() oncomplete: EventEmitter<number> = new EventEmitter();

	constructor(private service: MessageService) { }
	ngOnInit(): void { }
	confirm() {
		this.progress = true;
		this.service.insertMessage(this.obj).subscribe(
			op => {
				if (op.success) this.oncomplete.emit(1);
				else this.message = op.message;
			}, e => (this.progress = false), () => (this.progress = false)
		);
	}
	cancel() {
		this.oncomplete.emit(0);
	}
}
