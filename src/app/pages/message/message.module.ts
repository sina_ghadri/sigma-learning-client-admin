import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigmaSoftModule } from 'sigmasoft-ng';
import { FormsModule } from '@angular/forms';
import { MessageRoutingModule } from './message.routing';
import { MessageIndexComponent } from './index/index.component';
import { MessageCreateComponent } from './create/create.component';
import { MessageDetailComponent } from './detail/detail.component';
import { MessageEditComponent } from './edit/edit.component';
import { MessageDeleteComponent } from './delete/delete.component';
import { MessageService } from '../../services';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MessageRoutingModule,

    SigmaSoftModule
  ],
  declarations: [
    MessageIndexComponent,
    MessageCreateComponent,
    MessageDetailComponent,
    MessageEditComponent,
    MessageDeleteComponent,
  ],
  providers: [MessageService]
})
export class MessageModule { }
