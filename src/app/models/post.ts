export class Post {
    public id: number;
    public description: string = '';
    public body: string = '';
    public images: PostImage[] = [];

    get Images(): PostImage[] 
    { 
        if(!this.images) this.images = []; 
        if(this.images.length == 0) {
            let tmp = new PostImage;
            tmp.name = 'تصویر شاخص';
            tmp.usage = 'tumbnail';
            this.images.push(tmp); 
        }
        return this.images; 
    }
    set Images(value: PostImage[]) { this.images = value; }
}

export class PostImage {
    public name: string;
    public url: string;
    public usage: string;
    public width: number;
    public height: number;
}