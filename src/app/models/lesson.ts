import { Post } from "./post";
import { Course } from "./course";

export class Lesson {
    public id: number;
    public post: Post = new Post;
    public course: Course = new Course;
    public url: string;
    public time: number;
    public isFree: boolean;
}