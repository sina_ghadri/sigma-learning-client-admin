export class Category {
    public id: number;
    public name: string;
    public symbol: string;
    public parent: Category;
}