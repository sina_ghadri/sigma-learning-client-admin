export class Vipservice {
    public id: number;
    public name: string;
    public symbol: string;
    public price: number;
    public duration: number;
    public status: any;
}