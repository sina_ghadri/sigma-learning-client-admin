import { Post } from "./post";
import { User } from "./user";
import { KeyValue } from "sigmasoft-ts";

export class Course {
    public id: number;
    public post: Post = new Post;
    public teacher: User;
    public type: KeyValue;
    public status: KeyValue;

}

export enum CourseType {
    Free = 0,
    Vip = 1,
    OnSale = 2,
}

export enum CourseStatus {
    InQueue = 0,
    InProgress = 1,
    Completed = 2,
    Wait = 3,
}
