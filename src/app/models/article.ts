import { Post } from "./post";

export class Article {
    public id: number;
    public post: Post = new Post;
}