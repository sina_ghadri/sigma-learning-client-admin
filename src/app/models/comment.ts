export class Comment {
    public id: number;
}
export enum CommentStatus {
    Draft = 0,
    Approve = 1,
    Reject = 2,
    Delete = 3,
}