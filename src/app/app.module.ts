import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { BrowserModule } from "@angular/platform-browser";
import { SigmaSoftModule } from "sigmasoft-ng";

import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";
import { HttpConfigService } from "sigmasoft-ng/misc/jwt/http-config.service";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    SigmaSoftModule
  ],
  declarations: [AppComponent],
  providers: [HttpConfigService],
  bootstrap: [AppComponent]
})
export class AppModule {}
