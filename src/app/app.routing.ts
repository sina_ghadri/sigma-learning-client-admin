import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

const routes: Route[] = [
  { path: 'article', loadChildren: './pages/article/article.module#ArticleModule' },
  { path: 'category', loadChildren: './pages/category/category.module#CategoryModule' },
  { path: 'comment', loadChildren: './pages/comment/comment.module#CommentModule' },
  { path: 'course', loadChildren: './pages/course/course.module#CourseModule' },
  { path: 'discussion', loadChildren: './pages/discussion/discussion.module#DiscussionModule' },
  { path: 'lesson', loadChildren: './pages/lesson/lesson.module#LessonModule' },
  { path: 'message', loadChildren: './pages/message/message.module#MessageModule' },
  { path: 'post', loadChildren: './pages/post/post.module#PostModule' },
  { path: 'media', loadChildren: './pages/media/media.module#MediaModule' },
  { path: 'transaction', loadChildren: './pages/transaction/transaction.module#TransactionModule' },
  { path: 'user', loadChildren: './pages/user/user.module#UserModule' },
  { path: 'vipservice', loadChildren: './pages/vipservice/vipservice.module#VipserviceModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }