import { Component, OnInit } from '@angular/core';
import { Panel } from 'sigmasoft-ng/panel/admin/models';
import { JDate } from 'sigmasoft-ts';
import { HttpConfigService } from 'sigmasoft-ng/misc/jwt/http-config.service';
import { ConfigurationService } from 'sigmasoft-ng';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  panel: Panel;
  public constructor(private service: ConfigurationService, private httpConfig: HttpConfigService) {}
  ngOnInit(): void {

    this.service.loadConfiguration('config.json').subscribe(config => {
      this.panel = new Panel;
      this.panel.menus = config.menus;
      let data = JSON.parse(localStorage.getItem('loginInfo'));

      this.panel.onlogout.subscribe(() => location.href = config.login);
      this.httpConfig.on4xxUnAuthorize.subscribe(() => location.href = config.login);

      if(data) {
        this.panel.datetime = JDate.now;
        this.panel.module = data.modules.find(x => x.symbol == config.symbol);
        this.panel.modules = data.modules;
        this.panel.user = data.token.user;

        this.httpConfig.token = data.token.key;
      } else location.href = config.login;
    })
  }
}
